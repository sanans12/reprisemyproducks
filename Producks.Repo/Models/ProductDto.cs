﻿using System.ComponentModel;
using Producks.Data;

namespace Producks.Repo.Models
{
    public class ProductDto
    {
        public ProductDto() { }

        public ProductDto(Product product)
        {
            Id = product.Id;
            CategoryId = product.CategoryId;
            BrandId = product.BrandId;
            Name = product.Name;
            Description = product.Description;
            Price = product.Price;
            StockLevel = product.StockLevel;
            CategoryName = product.Category.Name;
            BrandName = product.Brand.Name;
        }

        public int Id { get; set; }
        public int CategoryId { get; set; }
        public int BrandId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        [DisplayName("Stock Level")]
        public int StockLevel { get; set; }
        [DisplayName("Category")]
        public string CategoryName { get; set; }
        [DisplayName("Brand")]
        public string BrandName { get; set; }
    }
}
