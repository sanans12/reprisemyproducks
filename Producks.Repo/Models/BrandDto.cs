﻿using Producks.Data;

namespace Producks.Repo.Models
{
    public class BrandDto
    {
        public BrandDto() { }

        public BrandDto(Brand brand)
        {
            Id = brand.Id;
            Name = brand.Name;
        }

        public int Id { get; set; }
        public string Name { get; set; }
    }
}
