﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Producks.Data;
using Producks.Repo.Models;

namespace Producks.Repo
{
    public class ProducksService : IProducksService
    {
        private readonly StoreDb _context;

        public ProducksService(StoreDb context)
        {
            _context = context;
        }

        #region Brands

        public async Task<List<BrandDto>> GetBrands()
        {
            return await _context.Brands.Where(brand => brand.Active)
                                                         .Select(brand => new BrandDto(brand))
                                                         .ToListAsync();
        }

        public async Task<BrandDto> GetBrand(int? id)
        {
            if (!id.HasValue) throw new ArgumentNullException(); 

            Brand brand = await _context.Brands.Where(x => x.Active)
                                               .FirstOrDefaultAsync(x => x.Id == id);

            return new BrandDto(brand);
        }

        public async Task DeleteBrand(int? id)
        {
            if (!id.HasValue) throw new ArgumentNullException();

            Brand brand = await _context.Brands.FindAsync(id);

            brand.Active = false;

            _context.Update(brand);
            await _context.SaveChangesAsync();
        }

        public async Task CreateBrand(BrandDto brandDto)
        {
            if (brandDto == null) throw new ArgumentNullException();

            Brand brand = new Brand
            {
                Id = brandDto.Id,
                Name = brandDto.Name,
                Active = true
            };

            _context.Add(brand);
            await _context.SaveChangesAsync();
        }

        public async Task EditBrand(BrandDto brandDto)
        {
            if (brandDto == null) throw new ArgumentNullException();

            Brand brand = new Brand()
            {
                Id = brandDto.Id,
                Name = brandDto.Name,
                Active = true
            };

            try
            {
                _context.Update(brand);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (_context.Brands.Any(x => x.Id == brand.Id))
                {
                   throw new ArgumentException();
                }

                throw;
            }
        }

        #endregion

        #region Categories

        public async Task<List<CategoryDto>> GetCategories()
        {
            return await _context.Categories.Where(category => category.Active)
                                                                    .Select(category => new CategoryDto(category))
                                                                    .ToListAsync();
        }

        public async Task<CategoryDto> GetCategory(int? id)
        {
            if (!id.HasValue) throw new ArgumentNullException();

            Category category = await _context.Categories.Where(x => x.Active)
                                                         .FirstOrDefaultAsync(x => x.Id == id);

            return new CategoryDto(category);
        }

        public async Task DeleteCategory(int? id)
        {
            if (!id.HasValue) throw new ArgumentNullException();

            Category category = await _context.Categories.FindAsync(id);

            category.Active = false;

            _context.Update(category);
            await _context.SaveChangesAsync();
        }

        public async Task CreateCategory(CategoryDto categoryDto)
        {
            if (categoryDto == null) throw new ArgumentNullException();

            Category category = new Category
            {
                Id = categoryDto.Id,
                Name = categoryDto.Name,
                Description = categoryDto.Description,
                Active = true
            };

            _context.Add(category);
            await _context.SaveChangesAsync();
        }

        public async Task EditCategory(CategoryDto categoryDto)
        {
            if (categoryDto == null) throw new ArgumentNullException();

            Category category = new Category()
            {
                Id = categoryDto.Id,
                Name = categoryDto.Name,
                Description = categoryDto.Description,
                Active = true
            };

            try
            {
                _context.Update(category);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (_context.Categories.Any(x => x.Id == category.Id))
                {
                    throw new ArgumentException();
                }

                throw;
            }
        }

        #endregion

        #region Products

        public async Task<List<ProductDto>> GetProducts(string categoryName, string brandName, double? priceFrom, double? priceTo)
        {
            return await _context.Products.Include("Brand")
                                          .Include("Category")
                                          .Where(product => product.Active)
                                          .Where(product => string.IsNullOrWhiteSpace(categoryName) || product.Category.Name == categoryName)
                                          .Where(product => string.IsNullOrWhiteSpace(brandName) || product.Brand.Name == brandName)
                                          .Where(product => !priceFrom.HasValue || product.Price >= priceFrom)
                                          .Where(product => !priceTo.HasValue || product.Price <= priceTo)
                                          .Select(product => new ProductDto(product))
                                          .ToListAsync();

        }

        public async Task<ProductDto> GetProduct(int? id)
        {
            if (!id.HasValue) throw new ArgumentNullException();

            Product product = await _context.Products.Where(x => x.Active)
                                                     .Include(x => x.Category)
                                                     .Include(x => x.Brand)
                                                     .FirstOrDefaultAsync(x => x.Id == id);

            return new ProductDto(product);
        }

        public async Task DeleteProduct(int? id)
        {
            if (!id.HasValue) throw new ArgumentNullException();

            Product product = await _context.Products.FindAsync(id);

            product.Active = false;

            _context.Update(product);
            await _context.SaveChangesAsync();
        }

        public async Task CreateProduct(ProductDto productDto)
        {
            if (productDto == null) throw new ArgumentNullException();

            Product product = new Product
            {
                Id = productDto.Id,
                CategoryId = productDto.CategoryId,
                BrandId = productDto.BrandId,
                Name = productDto.Name,
                Description = productDto.Description,
                Price = productDto.Price,
                StockLevel = productDto.StockLevel,
                Active = true,
                Category = await _context.Categories.FindAsync(productDto.CategoryId),
                Brand = await _context.Brands.FindAsync(productDto.BrandId)
            };

            _context.Add(product);
            await _context.SaveChangesAsync();
        }

        public async Task EditProduct(ProductDto productDto)
        {
            if (productDto == null) throw new ArgumentNullException();

            Product product = new Product
            {
                Id = productDto.Id,
                CategoryId = productDto.CategoryId,
                BrandId = productDto.BrandId,
                Name = productDto.Name,
                Description = productDto.Description,
                Price = productDto.Price,
                StockLevel = productDto.StockLevel,
                Active = true,
                Category = await _context.Categories.FindAsync(productDto.CategoryId),
                Brand = await _context.Brands.FindAsync(productDto.BrandId)
            };

            try
            {
                _context.Update(product);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (_context.Products.Any(x => x.Id == product.Id))
                {
                    throw new ArgumentException();
                }

                throw;
            }
        }

        #endregion

    }
}
