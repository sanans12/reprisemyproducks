﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Producks.Repo.Models;

namespace Producks.Repo
{
    public interface IProducksService
    {
        Task<List<BrandDto>> GetBrands();
        Task<BrandDto> GetBrand(int? id);
        Task DeleteBrand(int? id);
        Task CreateBrand(BrandDto brandDto);
        Task EditBrand(BrandDto brandDto);
        Task<List<CategoryDto>> GetCategories();
        Task<CategoryDto> GetCategory(int? id);
        Task DeleteCategory(int? id);
        Task CreateCategory(CategoryDto categoryDto);
        Task EditCategory(CategoryDto categoryDto);
        Task<List<ProductDto>> GetProducts(string categoryName, string brandName, double? priceFrom, double? priceTo);
        Task<ProductDto> GetProduct(int? id);
        Task DeleteProduct(int? id);
        Task CreateProduct(ProductDto productDto);
        Task EditProduct(ProductDto productDto);
    }
}