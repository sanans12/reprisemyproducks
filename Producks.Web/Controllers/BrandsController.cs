using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Producks.Repo;
using Producks.Repo.Models;

namespace Producks.Web.Controllers
{
    public class BrandsController : Controller
    {
        private IProducksService _producksService;

        public BrandsController(IProducksService producksService)
        {
            _producksService = producksService;
        }

        // GET: Brands
        public async Task<IActionResult> Index()
        {
            return View(await _producksService.GetBrands());
        }

        // GET: Brands/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            return await GetBrandDtoForView(id);
        }

        // GET: Brands/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Brands/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name")] BrandDto brandDto)
        {
            if (ModelState.IsValid)
            {
                await _producksService.CreateBrand(brandDto);

                return RedirectToAction(nameof(Index));
            }

            return View(brandDto);
        }

        // GET: Brands/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            return await GetBrandDtoForView(id);
        }

        // POST: Brands/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name")] BrandDto brandDto)
        {
            if (id != brandDto.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                await _producksService.CreateBrand(brandDto);

                return RedirectToAction(nameof(Index));
            }

            return View(brandDto);
        }

        // GET: Brands/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            return await GetBrandDtoForView(id);
        }

        // POST: Brands/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await _producksService.DeleteBrand(id);

            return RedirectToAction(nameof(Index));
        }

        private async Task<IActionResult> GetBrandDtoForView(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            BrandDto brandDto = await _producksService.GetBrand(id.Value);

            if (brandDto == null)
            {
                return NotFound();
            }

            return View(brandDto);
        }
    }
}
