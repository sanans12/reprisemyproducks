using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Producks.Repo;
using Producks.Repo.Models;

namespace Producks.Web.Controllers
{
    public class ProductsController : Controller
    {
        private IProducksService _producksService;

        public ProductsController(IProducksService producksService)
        {
            _producksService = producksService;
        }

        // GET: Products
        public async Task<IActionResult> Index(string categoryName, string brandName, double? priceFrom, double? priceTo)
        {
            await PopulateDropdownsByName();
            return View(await _producksService.GetProducts(categoryName, brandName, priceFrom, priceTo));
        }

        // GET: Products/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            return await GetProductDtoForView(id);
        }

        // GET: Products/Create
        public async Task<IActionResult> Create()
        {
            await PopulateDropdownsById();
            return View();
        }

        // POST: Products/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,CategoryId,BrandId,Name,Description,Price,StockLevel")] ProductDto productDto)
        {
            if (ModelState.IsValid)
            {
                await _producksService.CreateProduct(productDto);

                return RedirectToAction(nameof(Index));
            }

            return View(productDto);
        }

        // GET: Products/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            await PopulateDropdownsById();
            return await GetProductDtoForView(id);
        }

        // POST: Products/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,CategoryId,BrandId,Name,Description,Price,StockLevel")] ProductDto productDto)
        {
            if (id != productDto.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                await _producksService.EditProduct(productDto);

                return RedirectToAction(nameof(Index));
            }

            return View(productDto);
        }

        // GET: Products/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            return await GetProductDtoForView(id);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await _producksService.DeleteProduct(id);

            return RedirectToAction(nameof(Index));
        }

        private async Task PopulateDropdownsById()
        {
            ViewData["CategoryId"] = new SelectList(await _producksService.GetCategories(),"Id", "Name");

            ViewData["BrandId"] = new SelectList(await _producksService.GetBrands(), "Id", "Name");
        }

        private async Task PopulateDropdownsByName()
        {
            ViewData["CategoryName"] = new SelectList(await _producksService.GetCategories(), "Name", "Name");

            ViewData["BrandName"] = new SelectList(await _producksService.GetBrands(), "Name", "Name");
        }

        private async Task<IActionResult> GetProductDtoForView(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ProductDto productDto = await _producksService.GetProduct(id.Value);

            if (productDto == null)
            {
                return NotFound();
            }

            return View(productDto);
        }
    }
}
