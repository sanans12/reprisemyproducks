using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Producks.Repo;
using Producks.Repo.Models;

namespace Producks.Web.Controllers
{
    public class CategoriesController : Controller
    {
        private IProducksService _producksService;

        public CategoriesController(IProducksService producksService)
        {
            _producksService = producksService;
        }

        // GET: Categories
        public async Task<IActionResult> Index()
        {
            return View(await _producksService.GetCategories());
        }

        // GET: Categories/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            return await GetCategoryDtoForView(id);
        }

        // GET: Categories/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Categories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Description")] CategoryDto categoryDto)
        {
            if (ModelState.IsValid)
            {
                await _producksService.CreateCategory(categoryDto);

                return RedirectToAction(nameof(Index));
            }

            return View(categoryDto);
        }

        // GET: Categories/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            return await GetCategoryDtoForView(id);
        }

        // POST: Categories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Description")] CategoryDto categoryDto)
        {
            if (id != categoryDto.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                await _producksService.EditCategory(categoryDto);
                
                return RedirectToAction(nameof(Index));
            }

            return View(categoryDto);
        }

        // GET: Categories/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            return await GetCategoryDtoForView(id);
        }

        // POST: Categories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await _producksService.DeleteCategory(id);

            return RedirectToAction(nameof(Index));
        }

        private async Task<IActionResult> GetCategoryDtoForView(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            CategoryDto categoryDto = await _producksService.GetCategory(id.Value);

            if (categoryDto == null)
            {
                return NotFound();
            }

            return View(categoryDto);
        }
    }
}
