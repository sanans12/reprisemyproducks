﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Producks.Facades;
using Producks.Facades.Models;
using Producks.Repo;
using Producks.Repo.Models;
using Producks.Web.ViewModels;

namespace Producks.Web.Controllers
{
    public class StoreController : Controller
    {
        private readonly IProducksService _producksService;
        private IUndercuttersService _undercuttersService;

        public StoreController(IProducksService producksService, IUndercuttersService undercuttersService)
        {
            _producksService = producksService;
            _undercuttersService = undercuttersService;
        }

        public async Task<IActionResult> Index(string categoryName, string brandName, double? priceFrom, double? priceTo)
        {
            await PopulateDropdowns();
            return View(await GetProductViewModels(categoryName, brandName, priceFrom, priceTo));
        }

        private async Task<List<ProductViewModel>> GetProductViewModels(string categoryName = null, string brandName = null, double? priceFrom = null, double? priceTo = null)
        {
            List<ProductDto> productDtos = await _producksService.GetProducts(categoryName, brandName, priceFrom, priceTo);
            List<UCProductDto> ucProductDtos = await _undercuttersService.GetUnderCutterProducts(categoryName, brandName, priceFrom, priceTo);

            List<ProductViewModel> productViewModels = new List<ProductViewModel>();

            productDtos.ForEach(productDto => productViewModels.Add(new ProductViewModel(productDto)));
            ucProductDtos.ForEach(ucProductDto => productViewModels.Add(new ProductViewModel(ucProductDto)));

            return productViewModels;
        }

        private async Task PopulateDropdowns()
        {
            List<CategoryDto> categoryDtos = await _producksService.GetCategories();

            List<UCCategoryDto> ucCategoryDtos = await _undercuttersService.GetUnderCutterCategories();

            List<DropDownCategoryViewModel> dropDownCategoryViewModels = new List<DropDownCategoryViewModel>();

            categoryDtos.ForEach(categoryDto => dropDownCategoryViewModels.Add(new DropDownCategoryViewModel(categoryDto)));
            ucCategoryDtos.ForEach(ucCategoryDto =>
            {
                if (dropDownCategoryViewModels.All(x => x.Name != ucCategoryDto.Name))
                    dropDownCategoryViewModels.Add(new DropDownCategoryViewModel(ucCategoryDto));
            });

            ViewData["CategoryName"] = new SelectList(dropDownCategoryViewModels.ToList(), "Name", "Name");

            List<BrandDto> brandDtos = await _producksService.GetBrands();

            List<UCBrandDto> ucBrandDtos = await _undercuttersService.GetUnderCutterBrands();

            List<DropDownBrandViewModel> dropDownBrandViewModels = new List<DropDownBrandViewModel>();

            brandDtos.ForEach(brandDto => dropDownBrandViewModels.Add(new DropDownBrandViewModel(brandDto)));
            ucBrandDtos.ForEach(ucBrandDto => 
            { 
                if (dropDownBrandViewModels.All(x => x.Name != ucBrandDto.Name))
                        dropDownBrandViewModels.Add(new DropDownBrandViewModel(ucBrandDto));
            });

            ViewData["BrandName"] = new SelectList(dropDownBrandViewModels.ToList(), "Name", "Name");
        }
    }
}