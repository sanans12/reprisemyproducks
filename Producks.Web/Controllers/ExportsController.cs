﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Producks.Data;
using Producks.Repo;
using Producks.Repo.Models;
using Producks.Web.Models;

namespace Producks.Web.Controllers
{
    [ApiController, Route("api")]
    public class ExportsController : ControllerBase
    {
        private IProducksService _producksService;

        public ExportsController(IProducksService producksService)
        {
            _producksService = producksService;
        }

        [HttpGet("Brands")]
        public async Task<IActionResult> GetBrands()
        {
            List<BrandDto> brands = await _producksService.GetBrands();

            return Ok(brands);
        }

        [HttpGet("Brand")]
        public async Task<IActionResult> GetBrand(int? id)
        {
            try
            {
                BrandDto brand = await _producksService.GetBrand(id);
                return Ok(brand);
            }
            catch (ArgumentNullException)
            {
                return NotFound();
            }
        }

        [HttpGet("Categories")]
        public async Task<IActionResult> GetCategories()
        {
            List<CategoryDto> categories = await _producksService.GetCategories();

            return Ok(categories);
        }

        [HttpGet("Category")]
        public async Task<IActionResult> GetCategory(int? id)
        {
            try
            {
                CategoryDto category = await _producksService.GetCategory(id);
                return Ok(category);
            }
            catch (ArgumentNullException)
            {
                return NotFound();
            }
        }

        [HttpGet("Products")]
        public async Task<IActionResult> GetProducts(string categoryName, string brandName, double? priceFrom, double? priceTo)
        {
            List<ProductDto> products = await _producksService.GetProducts(categoryName, brandName, priceFrom, priceTo);

            return Ok(products);
        }

        [HttpGet("Product")]
        public async Task<IActionResult> GetProduct(int? id)
        {
            try
            {
                ProductDto product = await _producksService.GetProduct(id);
                return Ok(product);
            }
            catch (ArgumentNullException)
            {
                return NotFound();
            }
        }
    }
}
