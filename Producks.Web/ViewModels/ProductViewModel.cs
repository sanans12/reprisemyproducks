﻿using System.ComponentModel;
using Producks.Facades.Models;
using Producks.Repo.Models;

namespace Producks.Web.ViewModels
{
    public class ProductViewModel
    {
        private const string IN_STOCK_TEXT = "In Stock";
        private const string OUT_OF_STOCK_TEXT = "Out of Stock";

        public ProductViewModel(ProductDto product)
        {
            CategoryName = product.CategoryName;
            BrandName = product.BrandName;
            Name = product.Name;
            Description = product.Description;
            Price = product.Price;
            InStock = product.StockLevel > 0 ? IN_STOCK_TEXT : OUT_OF_STOCK_TEXT;
        }

        public ProductViewModel(UCProductDto product)
        {
            CategoryName = product.CategoryName;
            BrandName = product.BrandName;
            Name = product.Name;
            Description = product.Description;
            Price = product.Price;
            InStock = product.InStock ? IN_STOCK_TEXT : OUT_OF_STOCK_TEXT;
        }

        [DisplayName("Category")]
        public string CategoryName { get; set; }
        [DisplayName("Brand")]
        public string BrandName { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        [DisplayName("In Stock")]
        public string InStock { get; set; }
    }
}
