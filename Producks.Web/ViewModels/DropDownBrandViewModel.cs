﻿namespace Producks.Web.ViewModels
{
    public class DropDownBrandViewModel
    {
        public DropDownBrandViewModel(dynamic brand)
        {
            Id = brand.Id;
            Name = brand.Name;
        }

        public int Id { get; set; }
        public string Name { get; set; }
    }
}
