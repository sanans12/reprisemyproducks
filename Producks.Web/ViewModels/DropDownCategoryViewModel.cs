﻿namespace Producks.Web.ViewModels
{
    public class DropDownCategoryViewModel
    {
        public DropDownCategoryViewModel(dynamic category)
        {
            Id = category.Id;
            Name = category.Name;
        }

        public int Id { get; set; }
        public string Name { get; set; }
    }
}
