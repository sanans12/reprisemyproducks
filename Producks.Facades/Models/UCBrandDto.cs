﻿namespace Producks.Facades.Models
{
    public class UCBrandDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int AvailableProductCount { get; set; }
    }
}
