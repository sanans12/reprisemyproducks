﻿namespace Producks.Facades.Models
{
    public class UCCategoryDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int AvailableProductCount { get; set; }
    }
}
