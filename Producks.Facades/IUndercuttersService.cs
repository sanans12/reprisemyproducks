﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Producks.Facades.Models;

namespace Producks.Facades
{
    public interface IUndercuttersService
    {
        Task<List<UCProductDto>> GetUnderCutterProducts(string categoryName = null, string brandName = null, double? priceFrom = null, double? priceTo = null);
        Task<List<UCCategoryDto>> GetUnderCutterCategories();
        Task<List<UCBrandDto>> GetUnderCutterBrands();
    }
}