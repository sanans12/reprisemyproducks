﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Flurl;
using Flurl.Http;
using Producks.Facades.Models;

namespace Producks.Facades
{
    public class UndercuttersService : IUndercuttersService
    {
        private const string UNDERCUTTERS_URL = "http://undercutters.azurewebsites.net";

        public async Task<List<UCProductDto>> GetUnderCutterProducts(string categoryName = null, string brandName = null, double? priceFrom = null, double? priceTo = null)
        {
            Url url = UNDERCUTTERS_URL.AppendPathSegments("api", "product");

            if (!string.IsNullOrWhiteSpace(categoryName)) url.SetQueryParam("category_name", categoryName);
            if (priceFrom.HasValue) url.SetQueryParam("min_price", priceFrom);
            if (priceTo.HasValue) url.SetQueryParam("max_price", priceTo);

            HttpResponseMessage response = await url.GetAsync();

            List<UCProductDto> ucProductDtos = await response.Content.ReadAsAsync<List<UCProductDto>>();

            if (!string.IsNullOrWhiteSpace(brandName)) ucProductDtos = ucProductDtos.Where(x => x.BrandName == brandName).ToList();

            return ucProductDtos;
        }

        public async Task<List<UCCategoryDto>> GetUnderCutterCategories()
        {
            HttpResponseMessage response = await UNDERCUTTERS_URL.AppendPathSegments("api", "category").GetAsync();
            return await response.Content.ReadAsAsync<List<UCCategoryDto>>();
        }

        public async Task<List<UCBrandDto>> GetUnderCutterBrands()
        {
            HttpResponseMessage response = await UNDERCUTTERS_URL.AppendPathSegments("api", "brand").GetAsync();
            return await response.Content.ReadAsAsync<List<UCBrandDto>>();
        }
    }
}
